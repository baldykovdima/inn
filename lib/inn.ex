defmodule Inn do

  @multipliers10 [2,4,10,3,5,9,4,6,8]
  @multipliers12 [7,2,4,10,3,5,9,4,6,8]
  @multipliers12_1 [3,7,2,4,10,3,5,9,4,6,8]

  def check(inn) do
    case String.length(inn) do
      10 -> check10(inn)
      12 -> check12(inn)
      _ -> false
    end
  end

  def check10(inn) do
    controlNumber = controlNumber(@multipliers10, inn)

    String.at(inn, 9) === Integer.to_string(controlNumber)
  end

  def check12(inn) do
    controlNumber = controlNumber(@multipliers12, inn)
    controlNumber1 = controlNumber(@multipliers12_1, inn)

    String.at(inn, 10) === Integer.to_string(controlNumber) && String.at(inn, 11) === Integer.to_string(controlNumber1)
  end

  defp controlNumber(multipliers, inn) do
    multipliers
            |> Enum.with_index()
            |> Enum.map(fn {item, index} -> item * String.to_integer(String.at(inn, index)) end)
            |> Enum.sum()
            |> controlNumberByCheckSum()
  end

  defp controlNumberByCheckSum(checkSum) do
    controlNumber = checkSum - div(checkSum, 11) * 11
    if controlNumber === 10 do
      0
    end

    controlNumber
  end
end
