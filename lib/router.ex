defmodule PlugEx.Router do
    use Plug.Router

    alias Inn

    plug :match
    plug :dispatch
    plug Plug.Static, at: "/home", from: :server

    get "/" do
        send_resp(conn, 200, "Running")
    end

    get "/checkInn/:inn" do

        result = Inn.check(inn)
        send_resp(conn, 200, "#{result}")
    end

    get "/home" do
        conn = put_resp_content_type(conn, "type/html")
        send_file(conn, 200, "lib/index.html")
    end

    match _, do: send_resp(conn, 404, "Not found")
end
